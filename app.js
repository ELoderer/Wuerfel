const wuerfel = document.getElementById('wuerfel');
const wuerfelbutton = document.getElementById('wuerfelbutton');

function wuerfeln(event) {
    let zahl = Math.floor(Math.random()*6)+1
    if(zahl == 1) {
        wuerfel.innerHTML = "<svg height='200' width='200'><circle cx='100' cy='100' r='20' fill='black'/></svg>"
    }
    if(zahl == 2) {
        wuerfel.innerHTML = "<svg height='200' width='200'><circle cx='45' cy='45' r='20' fill='black'/><circle cx='155' cy='155' r='20' fill='black'/></svg>"
    }
    if(zahl == 3) {
        wuerfel.innerHTML = "<svg height='200' width='200'><circle cx='45' cy='45' r='20' fill='black'/><circle cx='100' cy='100' r='20' fill='black'/><circle cx='155' cy='155' r='20' fill='black'/></svg>"
    }
    if(zahl == 4){
        wuerfel.innerHTML = "<svg height='200' width='200'><circle cx='45' cy='45' r='20' fill='black'/><circle cx='45' cy='155' r='20' fill='black'/><circle cx='155' cy='45' r='20' fill='black'/><circle cx='155' cy='155' r='20' fill='black'/></svg>"
    }
    if(zahl == 5) {
        wuerfel.innerHTML = "<svg height='200' width='200'><circle cx='45' cy='45' r='20' fill='black'/><circle cx='45' cy='155' r='20' fill='black'/><circle cx='100' cy='100' r='20' fill='black'/><circle cx='155' cy='45' r='20' fill='black'/><circle cx='155' cy='155' r='20' fill='black'/></svg>"
    }
    if(zahl == 6) {
        wuerfel.innerHTML = "<svg height='200' width='200'><circle cx='45' cy='45' r='20' fill='black'/><circle cx='100' cy='45' r='20' fill='black'/><circle cx='155' cy='45' r='20' fill='black'/><circle cx='45' cy='155' r='20' fill='black'/><circle cx='100' cy='155' r='20' fill='black'/><circle cx='155' cy='155' r='20' fill='black'/></svg>"
    }
}

wuerfelbutton.addEventListener('click', wuerfeln);